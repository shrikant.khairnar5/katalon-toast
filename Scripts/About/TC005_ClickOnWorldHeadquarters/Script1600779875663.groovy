import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.URL as URL

try {
    CustomKeywords.'com.application.DateTime.startTime'()

    'Click on About link'
    WebUI.click(findTestObject('Object Repository/Toastmasters-About/span_About'))
    Thread.sleep(2000)
    WebUI.click(findTestObject('Object Repository/Toastmasters-About/a_World Headquarters'))
	
    'Get Page Name & URL'
    String headerName = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -About/h1_About'))
    String getUrl = WebUI.getUrl()
	
    if (headerName == 'WORLD HEADQUARTERS') {
        GlobalVariable.vartestresult = 'PASS'
        GlobalVariable.varpassremarks = ('User are able to click on World Headquarters link and URL is:' + getUrl)
    } else {
        GlobalVariable.vartestresult = 'FAIL'
        GlobalVariable.varfailremarks = 'User not able to click on World Headquarters link and URL is: '+getUrl
        WebUI.back()
    }
    
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)
    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
    println(e)
    GlobalVariable.vartestresult = 'FAIL'
    GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)

    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
}