import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.exception.StepErrorException as StepErrorException

try {
	CustomKeywords.'com.application.DateTime.startTime'()

	
	'Verify member is eligible to participate in the traditional program or not'
	Thread.sleep(2000)
	if (GlobalVariable.member == "Fail")
	   {
	   print "This test case is fail because of previous test case is fail"
	   }else
       {
	   print "pass"
	   'Verify Review Award Submission page'
	   WebUI.click(findTestObject('Object Repository/Submit Education Awards/button_Review award submission'))
	  
	   'Verify Review Award Submission page'
	   String headerName = WebUI.getText(findTestObject('Object Repository/Submit Education Awards/h2_Review Award Submission'))
	   Thread.sleep(1000);
	
	    if (headerName == "REVIEW AWARD SUBMISSION"){
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="REVIEW AWARD SUBMISSION page is open sucessfully."
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="REVIEW AWARD SUBMISSION page is not open sucessfully."
	}
	   }
	   
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}
