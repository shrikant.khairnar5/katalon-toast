import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

try {
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebUI.navigateToUrl(GlobalVariable.TestData["URL"]+"My-Toastmasters/profile/club-central")
	Thread.sleep(3000);
	
	'Verify Submit Eduction Awards Page'
	Thread.sleep(2000)
	WebUI.click(findTestObject('Object Repository/Page Club Central/Submit Education Awards                                                                                                                Submit member education awards'))
	Thread.sleep(2000)
	String headerName = WebUI.getText(findTestObject('Object Repository/Submit Education Awards/h2_Submit Education Awards'))
	Thread.sleep(1000);
	String getUrl = WebUI.getUrl();
	
	'Compare Current URL and Getting URL'
	if (headerName == "SUBMIT EDUCATION AWARDS"){
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Submit Eduction page is open sucessfully "+getUrl
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Submit Eduction page is not open sucessfully"
	}
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}