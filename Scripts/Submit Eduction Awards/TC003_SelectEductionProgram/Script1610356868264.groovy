import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

try {
	CustomKeywords.'com.application.DateTime.startTime'()

	
	'Select Eduction Program'
	Thread.sleep(2000)
	WebUI.click(findTestObject('Object Repository/Submit Education Awards/button_Select Education Program'))
	Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Submit Education Awards/button_Select Education Program'), Keys.chord(Keys.DOWN,Keys.DOWN, Keys.ENTER));
	Thread.sleep(1000)

	'Verify award for submission'
	if (WebUI.verifyElementVisible(findTestObject('Object Repository/Submit Education Awards/button_Review award submission'), FailureHandling.CONTINUE_ON_FAILURE)){
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.member="Pass"
		GlobalVariable.varpassremarks="Eduction Program is Selected"
		} else {
		GlobalVariable.vartestresult="FAIL"
		WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Submit Education Awards/This member not eligible to participate in traditional program'))
		GlobalVariable.member="Fail"
		GlobalVariable.varfailremarks="Eduction Program is not Selected because member joined after Pathways rolled out and is not eligible to participate in the traditional program"
	}
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}