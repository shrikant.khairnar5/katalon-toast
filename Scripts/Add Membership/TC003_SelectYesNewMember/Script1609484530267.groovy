import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Click on Yes,New Member Radio button '
	WebUI.click(findTestObject('Object Repository/Page_Add New Membership/Add Membership/input_Yes'))
	Thread.sleep(1000)
	
	
	if (WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Add New Membership/Add Membership/button_Submit'), FailureHandling.CONTINUE_ON_FAILURE))
	    {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="New Member Page is open sucessfully "
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="New Member Page is not open sucessfully."
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
