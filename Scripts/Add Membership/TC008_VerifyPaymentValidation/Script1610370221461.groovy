import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By


try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebDriver driver = DriverFactory.getWebDriver();
	
	'Click on Check box'
	WebUI.click(findTestObject('Object Repository/Page_Submit Payment for Member/Page_Toastmasters International -Submit Payment/Checkbox_Use Club Information'))
	Thread.sleep(1000)
	
	'Verify Credit card Validation with blank value'
	 String verifycardnumber = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/li_Please fill out this field'))
	 String verifyexpiredate = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/li_Please select an item in the list'))
	 Thread.sleep(2000)
	 
	 if (verifycardnumber == "Please fill out this field." &&  verifyexpiredate == "Please select an item in the list.")
		{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Please enter value of credit card snd select expire date"
		}
		else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="System allow to payment without enter credit card number and expire date"
		}
	
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}