import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By
import com.github.javafaker.Faker


try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebDriver driver = DriverFactory.getWebDriver();
	
	Faker faker = new Faker();
	
	GlobalVariable.firstName = faker.name().firstName()
	GlobalVariable.lastName = faker.name().lastName()
	GlobalVariable.memberEmail = faker.internet().emailAddress()
	
	'Enter Last Name with valid input'
	 WebUI.setText(findTestObject('NewMember/Page_Toastmasters International -Add Membership/input__LastName'),GlobalVariable.lastName )
	 Thread.sleep(2000)
	 
	 'Select Country From Country list'
	 WebUI.selectOptionByLabel(findTestObject('NewMember/Page_Toastmasters International -Add Membership/select_Country'),"United States" , true)
	 Thread.sleep(2000)
	 
	 
	 'Enter First Name with valid input'
	  WebUI.setText(findTestObject('NewMember/Page_Toastmasters International -Add Membership/input__FirstName'),GlobalVariable.firstName)
	  Thread.sleep(2000)

	
	 'Enter Company / In Care Of with valid  input'
	 WebUI.setText(findTestObject('NewMember/Page_Toastmasters International -Add Membership/input__Company'), GlobalVariable.TestData["Company"])
	 Thread.sleep(2000)
	
	 
	 'Enter Address Line 1 with valid input'
	 WebUI.setText(findTestObject('NewMember/Page_Toastmasters International -Add Membership/input__Address1'), GlobalVariable.TestData["AddressLine1"])
	 Thread.sleep(2000)
	 
	 'Select any one Gender'
	 WebUI.click(findTestObject('NewMember/Page_Toastmasters International -Add Membership/label_Male'))
	 Thread.sleep(1000)
	 
	 'Enter Address Line 2 with valid input'
	 WebUI.setText(findTestObject('NewMember/Page_Toastmasters International -Add Membership/input_Address2'), GlobalVariable.TestData["AddressLine2"])
	 Thread.sleep(2000)
	 
	 'Enter Home Phone with valid input'
	 WebUI.setText(findTestObject('Object Repository/Phones/Page_Toastmasters International -Add Membership/input_land Islands_HomePhone'), GlobalVariable.TestData["HomePhone"])
	 Thread.sleep(1000)
	 
	 'Enter Mobile Phone with valid input'
	 WebUI.setText(findTestObject('Object Repository/Phones/Page_Toastmasters International -Add Membership/input_land Islands_MobilePhone'), GlobalVariable.TestData["MobilePhone"])
	 Thread.sleep(1000)
	 
	 
	 'Enter City with valid input'
	 WebUI.setText(findTestObject('Object Repository/PendingForm2/Page_Toastmasters International -Add Membership/input__City'), GlobalVariable.TestData["City"])
	 Thread.sleep(1000)
	 
	 'Select any one State'
	 WebUI.click(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/select_SelectState'))
	 Thread.sleep(2000)
	 WebUI.selectOptionByValue(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/select_SelectState'), "NY", true)
	 Thread.sleep(1000)
	 
	 
	 
	 'Enter Primary Email with valid input'
	 WebUI.setText(findTestObject('Object Repository/PendingForm2/Page_Toastmasters International -Add Membership/input__PrimaryEmail'), GlobalVariable.memberEmail)
	 Thread.sleep(1000)
	 
	 'Click on "Add Another Email ID" Link'
	 WebUI.click(findTestObject('Object Repository/PendingForm2/Page_Toastmasters International -Add Membership/a_Add another email address'))
	 
	 'Enter Secondary Email with valid input'
	 WebUI.setText(findTestObject('Object Repository/PendingForm2/Page_Toastmasters International -Add Membership/input_SecondaryEmail'), GlobalVariable.TestData["SecondaryEmail"])
	 Thread.sleep(1000)	 
	 
	 'Enter ZipCode with valid input'
	 WebUI.setText(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/input__PostalCode'), GlobalVariable.TestData["ZipCode"])
	 Thread.sleep(1000)
	 
	 'Click on Conacting options'
	 WebUI.click(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/button_No'))
	 Thread.sleep(2000)
	 WebUI.click(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/button_Yes'))
	 Thread.sleep(2000)
	 
	 'Select Term and condition check box'
	 WebUI.click(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/input_Yes_IsVisuallyImpairedMember'))
	 Thread.sleep(2000)
	 WebUI.click(findTestObject('Page_NewMembershipFields/Page_Toastmasters International -Add Membership/input_Term_condition'))
	 Thread.sleep(2000)
	 
	 'Click on Submit button'
	 WebUI.click(findTestObject('Object Repository/Page_Add New Membership/Add Membership/button_Submit'))
	 Thread.sleep(5000)
	 
	 
	 'Verify Form submission'
	  if (WebUI.verifyElementVisible(findTestObject('Page_Add New Membership/Add Membership/a_Submit Payment'),FailureHandling.CONTINUE_ON_FAILURE))
		{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="New Member form is submitted"
		}
		else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="New Member form is not submitted"
		}
	
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}