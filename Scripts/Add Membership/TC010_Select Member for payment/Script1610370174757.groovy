import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys as Keys

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Scroll the screen down'
	WebUI.scrollToPosition(50, 60)
	
//	'Verify New Member is added or not'
//	String fullName1 = GlobalVariable.firstName + " " + GlobalVariable.lastName
//	println fullName1
//	Thread.sleep(2000)
//	String fullName2 = WebUI.getText(findTestObject('Object Repository/Page_Review Cart/Page_Toastmasters International -Submit Payment/td_Member')).split("\n")[0] 
//	println fullName2
//	Thread.sleep(2000)
//	if (fullName1 == fullName2)
//	{
//    println "Added New Member is matched"
//	}
//	else {
//	println "Added new member is not matched"
//	}
	
	Thread.sleep(5000)
	'Select Start Date'
	WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_Start Date'))
    Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_Start Date'), Keys.chord(Keys.DOWN, Keys.ENTER));
	Thread.sleep(3000);
	
	'Select End Date'
	WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_End Date'))
	Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_End Date'), Keys.chord(Keys.DOWN, Keys.ENTER));
	Thread.sleep(3000);
	
	'Click on Add to Cart button'
	//WebUI.sendKeys(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_End Date'), Keys.chord(Keys.DOWN, Keys.ENTER));
    WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/Add to Cart Button')  )
	Thread.sleep(3000);
	
	'Click on Remove link'
	WebUI.click(findTestObject('Object Repository/Page_Review Cart/Page_Toastmasters International -Submit Payment/a_Remove'))
	Thread.sleep(3000);
	 
	'Again Select Start Date'
	WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_Start Date'))
    Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_Start Date'), Keys.chord(Keys.DOWN, Keys.ENTER));
	Thread.sleep(3000);
	
	'Again Select End Date'
	WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_End Date'))
	Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/select_End Date'), Keys.chord(Keys.DOWN, Keys.ENTER));
	Thread.sleep(3000);
	
	'Again Click on Add to Cart button'
    WebUI.click(findTestObject('Object Repository/Page_AddMember for payment/Page_Toastmasters International -Submit Payment/Add to Cart Button')  )
	Thread.sleep(3000);
	
	'Click on Continue to Payment informatiom'
	WebUI.click(findTestObject('Object Repository/Page_Review Cart/Page_Toastmasters International -Submit Payment/Continue to Payment button'))
	Thread.sleep(3000)
	
	'Verify Cart text and Edit Cart button'
	String verifyCart = WebUI.getText(findTestObject('Object Repository/Page_Review Cart/Page_Toastmasters International -Submit Payment/h3_Review Cart'))
	Thread.sleep(2000)
	WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Review Cart/Page_Toastmasters International -Submit Payment/Button_Edit Cart'))
	Thread.sleep(2000)
	
	 if (verifyCart == "Review Cart")
		{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Member is added sucessfully for payment."
		}
		else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Member is added sucessfully for payment, please verify"
		}
	
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}