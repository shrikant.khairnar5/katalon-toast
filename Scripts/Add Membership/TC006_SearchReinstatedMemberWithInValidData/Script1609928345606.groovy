import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper as WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor as JavascriptExecutor
import org.openqa.selenium.By as By
import com.github.javafaker.Faker as Faker

try {
    CustomKeywords.'com.application.DateTime.startTime'()

    WebDriver driver = DriverFactory.getWebDriver()

    'Enter Last Name with Invalid input'
    WebUI.setText(findTestObject('Object Repository/SearchMember/Page_Toastmasters International -Add Membership/input__SearchLastName'), Lastname)
    Thread.sleep(2000)
	
    'Enter Primary Email with Invalid input'
    WebUI.setText(findTestObject('Object Repository/SearchMember/Page_Toastmasters International -Add Membership/input__SearchText'), Emailaddress)
    Thread.sleep(1000)
	
    'Click on search button'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Add Membership/button_Search'))
	Thread.sleep(2000)
	
	
    'Verify member not found'
	String membernotfound = WebUI.getText(findTestObject('Object Repository/SearchMember/Page_Toastmasters International -Add Membership/text_Member not found'))
	Thread.sleep(2000)
	
    if (membernotfound == "Member not found. Enter as a New Member or if you believe this individual to be a dual/reinstated member, please try an alternate email address or contact Club and Member Support for assistance.") 
	{
        GlobalVariable.vartestresult = 'PASS'
		'Click on cross button of pop-up'
		WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Add Membership/Cross button'))
        GlobalVariable.varpassremarks = 'Member not found'
    } else {
        GlobalVariable.vartestresult = 'FAIL'
        GlobalVariable.varfailremarks = 'Please check entered data and again try to search with'
    }
    
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
        GlobalVariable.varfailremarks)
        CustomKeywords.'com.application.DateTime.endTime'()
        CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
    println(e)
    GlobalVariable.vartestresult = 'FAIL'
    GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)
    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
} 

