import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	WebDriver driver = DriverFactory.getWebDriver();
	
	'Verify Payment Page'
	WebUI.getText(findTestObject('Object Repository/Page_Submit Payment for Member/Page_Toastmasters International -Submit Payment/h3_Payment Information'))
	Thread.sleep(2000)
	
	'Add Credit Card Details'
	WebUI.setText(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/input__Address'), GlobalVariable.TestData["CardNumber"])
	Thread.sleep(2000)
	
	WebUI.sendKeys(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/input__Address'), Keys.chord(Keys.CONTROL, 'a'))
	Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/input__Address'), Keys.chord(Keys.CONTROL, 'x'))
	Thread.sleep(2000)
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/input__CreditCardNumber'))
	Thread.sleep(2000)
	WebUI.sendKeys(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/input__CreditCardNumber'), Keys.chord(Keys.CONTROL, 'v'))
	Thread.sleep(2000)
	WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/select_Month'), GlobalVariable.TestData["ExpireDate"], true)
    Thread.sleep(2000)
	WebUI.selectOptionByValue(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/select_Year'), GlobalVariable.TestData["ExpireYear"], true)
	Thread.sleep(2000)
	
	'Click on check box of  Use Club Information'
	WebUI.click(findTestObject('Object Repository/Page_Submit Payment for Member/Page_Toastmasters International -Submit Payment/Checkbox_Use Club Information'))
	Thread.sleep(1000)
	
	'Click on Submit Payment Button'
	WebUI.click(findTestObject('Object Repository/Page_Submit Payment for Member/Page_Toastmasters International -Submit Payment/Button_Submit Payment'))
	Thread.sleep(8000)
	
	'Verify payment is sucessfully done or not'
	String verifypayment = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Submit Payment/div_Thank you. Your order has been placed successfully.  Congratulations'))
	print "Payment"+verifypayment
	Thread.sleep(2000)
		 
	    if (verifypayment == "Thank you. Your order has been placed successfully. Congratulations.")
		{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Your order has been placed successfully"
		}
		else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Your order has been not placed successfully"
		}
	
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}