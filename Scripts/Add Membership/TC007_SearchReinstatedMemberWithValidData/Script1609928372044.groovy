import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

try {
	CustomKeywords.'com.application.DateTime.startTime'()

	
	'Enter Last Name with valid input'
	WebUI.setText(findTestObject('Object Repository/SearchMember/Page_Toastmasters International -Add Membership/input__SearchLastName'), GlobalVariable.lastName)
	Thread.sleep(2000)
	
	'Enter Primary Email with valid input'
	WebUI.setText(findTestObject('Object Repository/SearchMember/Page_Toastmasters International -Add Membership/input__SearchText'), GlobalVariable.memberEmail)
	Thread.sleep(1000)
	
	'Click on search button'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Add Membership/button_Search'))
	Thread.sleep(2000)
	
	'Verify member is already exist'
	String memberalreadyexist = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Add Membership/div_This member is already associated'))
	Thread.sleep(2000)
	
	if (memberalreadyexist == "This member is already associated with this club. You may go to Submit Payment now to pay dues.")
	{
		GlobalVariable.vartestresult = 'PASS'
		WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Add Membership/Cross Button with existing member pop-up'))
		GlobalVariable.varpassremarks = 'Member is found'
	} else {
		GlobalVariable.vartestresult = 'FAIL'
		GlobalVariable.varfailremarks = 'Please check entered data and again try to search with'
	}
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
		GlobalVariable.varfailremarks)
		CustomKeywords.'com.application.DateTime.endTime'()
		CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}