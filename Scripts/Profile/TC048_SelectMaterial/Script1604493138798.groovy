import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Select or Veify English Language'
	Thread.sleep(3000)
	Languagetype = WebUI.getAttribute(findTestObject('Object Repository/TodayNewOR/Page_Toastmasters International -Select Your Preference/select_Language'),'value')
	Thread.sleep(2000)
	if (Languagetype == 'en-US')
	{	
	} else {
	 WebUI.click(findTestObject('Object Repository/TodayNewOR/Page_Toastmasters International -Select Your Preference/select_Language'))
	 WebUI.selectOptionByValue(findTestObject('Object Repository/TodayNewOR/Page_Toastmasters International -Select Your Preference/select_Language'), "en-US", false)
	 }
	
	'Select Digital Resource'
	WebUI.click(findTestObject('Object Repository/Page_Select Materials/Page_Toastmasters International -Select Your Preference/div_Select Digital Resources'))
	Thread.sleep(2000)
	String getUrl = WebUI.getUrl();
				
	if (WebUI.verifyElementPresent(findTestObject('Object Repository/Page_Select Materials/Page_Toastmasters International -Select Your Preference/div_Select Digital Resources_pathways-check-icon'),2))
	    {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Digital Resource is Selected by User."+getUrl
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Digital Resource is not Selected by User."
	}
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}