import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By


try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebDriver driver = DriverFactory.getWebDriver();
	
    'Click on Select Another Address Link '
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_(Select Another Address)'))
	
	'Click on Create New Address Button'
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Create New Address'))
	
	'Click on Next Page'
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Next'))
	String validate = WebUI.getText(findTestObject('Page_FillupAddress/Page_Checkout/label_This field is required'))
	String validatezipcode = WebUI.getText(findTestObject('Page_FillupAddress/Page_Checkout/label_Zip  Postal Code is invalid'))
	
	
	if (validate == "This field is required" && validatezipcode == "Zip / Postal Code is invalid")
	    {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Please enter mandatory fields."
		}
		else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Please add validation forall mandatory address fields."
		}
	
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}