import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Click on Photo Image'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/img'))
	Thread.sleep(2000)
	
	'Delete Present Profile Image'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/button_Delete profile photo'))
	Thread.sleep(2000)
	
	'Click on again Photo Image'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/img'))
	Thread.sleep(2000)
	
	if (WebUI.verifyElementNotVisible(findTestObject('Object Repository/Page_Toastmasters International -Profile/button_Delete profile photo')))
	{
			GlobalVariable.vartestresult="PASS"
			GlobalVariable.varpassremarks="Profile Image Deleted Sucessfully."
			}
			else {
			GlobalVariable.vartestresult="FAIL"
			GlobalVariable.varfailremarks="Profile Image not Delete Sucessfully."
			WebUI.back()
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}