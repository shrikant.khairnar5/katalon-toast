import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By

try {
	CustomKeywords.'com.application.DateTime.startTime'()
	WebDriver driver = DriverFactory.getWebDriver();
	
	
	'Payment Method- Enter Credit Card Details'
    //Enter Card Number
	Thread.sleep(2000)
	WebUI.setText(findTestObject('Page_CreditCard/Page_PaymentWithCreditCard/input__number'), GlobalVariable.TestData["CardNumber"])
	String cardnumber = driver.findElement(By.name("number")).getAttribute("value")
	if (cardnumber == "4111 1111 1111 1111")
	{
      println "Card number is valid"
	}else{
	  println "Please enter valid card number"
	}
	
	
	//Enter Expiration Date
	Thread.sleep(2000)
	WebUI.setText(findTestObject('Page_CreditCard/Page_PaymentWithCreditCard/input__expiration'),GlobalVariable.TestData["ExpirationDate"])
	String expirationdate = driver.findElement(By.name("expiration")).getAttribute("value")
	if (expirationdate == "1223")
	{
		println "Expirationdate is valid"
	} else{
	    println "Expirationdate is not valid"
	}
	
	
	
	//Enter CVV
	Thread.sleep(3000)
	WebUI.setText(findTestObject('Page_CreditCard/Page_PaymentWithCreditCard/input__cvc'), GlobalVariable.TestData["CVV"])
	String cvv = driver.findElement(By.name("cvc")).getAttribute("value")
	if (cvv == "1234")
	{
		println "CVV Is valid"
	} else {
        println "Please enter Valid cvv"	
 	}
	
	
	//Click Next button & Place Order
	Thread.sleep(3000)
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Next (1)'))
	String placeorder = WebUI.getText(findTestObject('Object Repository/ReviewAndPlaceOrder/Page_Checkout/p_Please review your above order information. When finished, press the button below to place your order'))
    //WebUI.click(findTestObject('Object Repository/ReviewAndPlaceOrder/Page_Checkout/a_Place Your Order'))
	
	if ( placeorder == "Please review your above order information. When finished, press the button below to place your order")
	{
		GlobalVariable.vartestresult="PASS"
		WebUI.click(findTestObject('Object Repository/ReviewAndPlaceOrder/Page_Checkout/a_Place Your Order'))
		GlobalVariable.varpassremarks="Credit Card details Enter sucessfully."
		}
	else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Please Enter Valid Credit Card details."
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}