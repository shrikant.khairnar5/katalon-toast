import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebElement;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

try {
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Init the driver'
	WebDriver driver = DriverFactory.getWebDriver();
	
	'Get the grid record are availble or not on screen'
	gridValue = driver.findElement(By.id("sponsorship_history_info")).getText()
	Thread.sleep(1000)
	
	if (gridValue == "Showing 0 to 0 of 0 entries") {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks='Record not available in grid and Status is: '+gridValue
	} 
	else {
	valueGrid = driver.findElement(By.xpath("//*[@id='sponsorship_history']/tbody/tr/td[1]")).getText() 
	Thread.sleep(1000)	
	driver.findElement(By.xpath("//*[@id='sponsorship_history_filter']/label/input")).sendKeys(valueGrid)
	Thread.sleep(1000)
	newGridValue = driver.findElement(By.xpath("//*[@id='sponsorship_history']/tbody/tr/td[1]")).getText()
	Thread.sleep(1000)
	
	if (newGridValue == valueGrid) {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Search Valid Club Number functionality working as per expected and Club Number is : "+valueGrid
	} 
	else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Search Club number functionality not working."
	}
	
}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}
