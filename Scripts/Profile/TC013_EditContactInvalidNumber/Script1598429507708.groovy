import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.MalformedURLException as MalformedURLException
import java.net.URL as URL
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Click on Edit Contact option'
	WebUI.click(findTestObject('Object Repository/Toastmasters-Profile/a_Edit contact information and privacy'))
	Thread.sleep(2000)
	
	'Click on Contact Information tab'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/a_Contact Information'))
	Thread.sleep(1000)
	
	'Set text in Home number field'
	WebUI.setText(findTestObject('Object Repository/Page_Toastmasters International -Profile/input_land Islands_ContactChangeHomeNumber'), "ABCD")
	Thread.sleep(1000)
	
	'Click on Save Contact Information button'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/button_Save Contact Information'))
	Thread.sleep(2000)
	String errorMsg = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Profile/div_Were sorry'))
	Thread.sleep(1000)
	//(949) 280-7799
	
	if (WebUI.verifyElementVisible(findTestObject('Object Repository/Page_Toastmasters International -Profile/div_Were sorry'), FailureHandling.CONTINUE_ON_FAILURE)) {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Validation message prompt for mobile number & message is: "+errorMsg
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Validation message not prompt for mobile number."
	}	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}