import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By 


try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	 WebDriver driver = DriverFactory.getWebDriver();
     Thread.sleep(2000)
	 String optionLabel = driver.findElement(By.xpath("//*[@id='mainContent']/div[1]/div[2]/div[1]/div[1]/div/div/h4")).getText()
	 Thread.sleep(2000)
	 driver.findElement(By.xpath("//*[@id='mainContent']/div[1]/div[2]/div[1]/div[1]/div/div/div[2]")).click()
	 
	 Thread.sleep(2000)
	 driver.findElement(By.id("loading-image")).click()
	 
	 Thread.sleep(2000)
	 String optionLabel1 = driver.findElement(By.xpath("//*[@id='mainContent']/div[1]/div[3]/div/div/div/h4")).getText()

	 
	 if (optionLabel == optionLabel1) {
	 println "Selected Value is matched"
	 WebUI.click(findTestObject('Object Repository/ContinueButton/Page_Toastmasters International -Select Path/button_Continue'))
	 } else {
	 println "Selected Value is not matched"
	 }
	
   /* 'MAKE YOUR SELECTION'
	WebUI.click(findTestObject('null'))
	Thread.sleep(1000)
	String headerName1 = WebUI.getText(findTestObject('null'))
	println "Selection is"+headerName1
	String getUrl = WebUI.getUrl(); 
	
	'Click Continue Button'
	WebUI.click(findTestObject('Object Repository/ContinueButton/Page_Toastmasters International -Select Path/button_Continue'))
    Thread.sleep(3000)
	
	'Compare Selection'
	String headerName2 = WebUI.getAttribute(findTestObject('null','text',2))
	println "Confirm Selection is"+headerName2
	
	'Compare text in next page'
	if (headerName1 == headerName2){
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Selected Option is same"+getUrl
		WebUI.click(findTestObject('Object Repository/NewCompareValue/Page_Toastmasters International -Select Path/button_Continue'))
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Selected Option is not same."
	}*/
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}