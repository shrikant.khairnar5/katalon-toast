import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.By

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebDriver driver = DriverFactory.getWebDriver();
	
    /*'Click on Select Another Address Link '
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_(Select Another Address)'))
	
	'Click on Create New Address Button'
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Create New Address'))
	
	'Click on Next Page'
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Next'))
	String validate = WebUI.getText(findTestObject('Page_FillupAddress/Page_Checkout/label_This field is required'))
	if (validate == "This field is required")
	  {
		 System.out.println ("Please enter mandatory fields")
	  } else { 
	     System.out.println ("Please add validation for all mandatory address fields") 
	  }*/
	
	
	'Fill-up All Fields of Address'
	//Select Country
	Thread.sleep(3000)
    WebUI.selectOptionByLabel(findTestObject('Page_FillupAddress/Page_Checkout/select_Country'),"United States", true)
	String Countryname = driver.findElement(By.name("country")).getAttribute("value")
	Thread.sleep(2000)
	if (Countryname == 'United States')
	{	
	  println "Country OF US is selected"
	} else {
	  println "Country is not selected"
	 }
	
	
	//Enter Address
	Thread.sleep(2000)
	WebUI.setText(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/input__address'),GlobalVariable.TestData["Address"])
	String address = driver.findElement(By.name("address")).getAttribute("value")
	if (address == "O-422,Mapple Trade center")
	   {
	   println "Address is correct"
	   } else 
   
       {
	   println "Please check address field"
	   }
	   
	//Enter City
    Thread.sleep(3000)
	WebUI.setText(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/input__city'), GlobalVariable.TestData["City"])
	String city = driver.findElement(By.name("city")).getAttribute("value")
	 if (city == "UK")
	   {
	   println "city is correct"
	   } else 
   
       {
	   println "Please check city field"
	   }
	   
	   
	   //Select State
	   Thread.sleep(3000)
	   WebUI.selectOptionByValue(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/select_State'),"17", true)
	   String state = driver.findElement(By.name("state")).getAttribute("value")
	   Thread.sleep(2000)
	   if (state == '17')
	   {
		 println "State Of Hawaii is selected"
	   } else {
		 println "State Of Hawaii is not selected"
		}
		  
	   
	   //Enter Zip Code
	   Thread.sleep(2000)
	   WebUI.setText(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/input__zip'),GlobalVariable.TestData["ZipCode"])
	   String zipcode = driver.findElement(By.name("zip")).getAttribute("value")
	   if (zipcode == "967123")
		  {
		  println "You have Enter correct Zipcode"
		  } else
	  
		  {
		  println "Zipcode is invalid"
		  }
	   

		  
    //Enter & select Phone Number 
	Thread.sleep(2000)
	WebUI.click(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/span_United States'))
	WebUI.setText(findTestObject('Object Repository/Page_EnterNewAddress/Page_Checkout/input_land Islands_phone'), GlobalVariable.TestData["PhoneNumber"])
	String phonenumber = driver.findElement(By.name("phone")).getAttribute("value")
	if (phonenumber == "(949) 280-6809")
	{
		println "Phone number is correct"
	}
	else
	{
		println "Phone number is not correct"
	}
	
	
	
    '2 Times Click on Next Page'
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Next'))
	Thread.sleep(3000)
	WebUI.click(findTestObject('Page_FillupAddress/Page_Checkout/a_Next'))
	
	
	'Verify Address is correct'
	if (WebUI.getText(findTestObject('Page_FillupAddress/Page_Checkout/h2_Payment Method')))
			{
			GlobalVariable.vartestresult="PASS"
			GlobalVariable.varpassremarks="Address is correct and enter credit card details."
			}
			else {
			GlobalVariable.vartestresult="FAIL"
			GlobalVariable.varfailremarks="Address is not correct,kindly check address."
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}