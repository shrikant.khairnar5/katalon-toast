import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import org.openqa.selenium.WebElement;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;

try {
	CustomKeywords.'com.application.DateTime.startTime'()
	
	
	'Click on Select Program option'
	WebUI.click(findTestObject('Object Repository/Toastmasters-Education Awards/button_Select Club'))
	Thread.sleep(2000)
	
	WebUI.sendKeys(findTestObject('Object Repository/Toastmasters-Education Awards/button_Select Club'), Keys.chord(Keys.ARROW_DOWN, Keys.ENTER))
	Thread.sleep(2000)
	
	'Get Dropdown valua in variable'
	clubName = WebUI.getText(findTestObject('Object Repository/Toastmasters-Education Awards/button_Select Club')).trim()
		
	'Get grid value in variale'	
	gridclubName = WebUI.getText(findTestObject('Object Repository/Toastmasters-Education Awards/td_Im Just Sayin'))
		
	'Compare the Dropdown value and grid value'
	if (clubName == gridclubName) {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Selected Club record displayed in grid & Selected Club Name is: "+clubName
		WebUI.click(findTestObject('Object Repository/Toastmasters-Education Awards/button_Clear Filters'))
	} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Selected Club record not displayed in grid."
	}
	
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}
