import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.JavascriptExecutor

try {
	CustomKeywords.'com.application.DateTime.startTime'()
	
	WebDriver driver = DriverFactory.getWebDriver();
	JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
	WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Toastmasters-About/a_WelcomeWally'), 30);
	jsExecutor.executeScript("arguments[0].click();", element);
	Thread.sleep(2000)
	
	'Click on My Downloads link'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/a_Individual Sponsorship History'))
	Thread.sleep(2000)
	String headerName = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Offices Held/h2_Offices Held'))
	String getUrl = WebUI.getUrl();
	String gridDate = WebUI.getText(findTestObject('Object Repository/Page_Toastmasters International -Membership History/div_Showing 1 to 2 of 2 entries'))
	
	if (headerName == "INDIVIDUAL SPONSORSHIP HISTORY")
	{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="User are able to click on INDIVIDUAL SPONSORSHIP HISTORY link and URL is:"+getUrl +
		" & grid record is: "+gridDate
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="User not able to click on INDIVIDUAL SPONSORSHIP HISTORY link."
		WebUI.back();
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	
}
catch (Exception e) {
	println(e)
	GlobalVariable.vartestresult = 'FAIL'
	GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks,
	GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
}