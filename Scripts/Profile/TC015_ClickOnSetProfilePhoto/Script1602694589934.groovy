import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.MalformedURLException as MalformedURLException
import java.net.URL as URL
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.driver.DriverFactory
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys as Keys

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Click on Profile link'
	WebDriver driver = DriverFactory.getWebDriver();
	JavascriptExecutor jsExecutor = (JavascriptExecutor)driver;
	WebElement element = WebUiCommonHelper.findWebElement(findTestObject('Toastmasters-About/a_WelcomeWally'), 30);
	jsExecutor.executeScript("arguments[0].click();", element);
	Thread.sleep(2000)
	
	'Click on Photo Image'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/img'))
	Thread.sleep(2000)
	
	'Click on Set as Profile Photo button'
	WebUI.click(findTestObject('Object Repository/Page_Toastmasters International -Profile/button_Set as profile photo'))
	Thread.sleep(5000)
	
	if (WebUI.verifyElementNotVisible(findTestObject('Object Repository/Page_Toastmasters International -Profile/button_Delete profile photo'))) 
	{
			GlobalVariable.vartestresult="PASS"
			GlobalVariable.varpassremarks="Profile page displayed on screen."
			} 
			else {
			GlobalVariable.vartestresult="FAIL"
			GlobalVariable.varfailremarks="Exception displayed on screen."
			WebUI.back()
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}