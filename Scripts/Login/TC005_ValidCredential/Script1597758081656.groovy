
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.MalformedURLException as MalformedURLException
import java.net.URL as URL

try
{
	CustomKeywords.'com.application.DateTime.startTime'()
	
	'Enter the valid username and password'
	WebUI.setText(findTestObject('Object Repository/Toastmasters-Login/input__DetailsUserName'), GlobalVariable.TestData["UserName"])
	Thread.sleep(1000)
	WebUI.setText(findTestObject('Object Repository/Toastmasters-Login/input__DetailsPassword'), GlobalVariable.TestData["Password"])
	Thread.sleep(1000)
	
	'Click on Login button'
	WebUI.click(findTestObject('Object Repository/Toastmasters-Login/input__btnLogin'))
	Thread.sleep(1000)
	
	String WelcomeText = WebUI.getText(findTestObject('Toastmasters-About/a_WelcomeWally'))
	Thread.sleep(1000)	
	
	if (WebUI.verifyElementVisible(findTestObject('Toastmasters-About/a_Logout'), FailureHandling.CONTINUE_ON_FAILURE)) {
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Logout link is visibale on screen & Welcome text is: "+WelcomeText
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Login functionality not working with valid credential."
		Thread.sleep(2000)
	}
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}
	catch(Exception e)
	{
	println(e)
	GlobalVariable.vartestresult="FAIL"
	GlobalVariable.varfailremarks= "Exception: "+e.getStackTrace().toString()
	CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, GlobalVariable.varfailremarks)
	CustomKeywords.'com.application.DateTime.endTime'()
	CustomKeywords.'com.application.DateTime.totalTime'()
	}