import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import javax.media.PackageManager as PackageManager
import javax.naming.Context as Context
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import com.kms.katalon.core.util.internal.PathUtil as PathUtil
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.MalformedURLException as MalformedURLException
import java.net.URL as URL

try {
    CustomKeywords.'com.application.DateTime.startTime'()
	
	'Click on Login link'
	WebUI.click(findTestObject('Object Repository/Toastmasters-Home/a_Login'))
	Thread.sleep(1000)
	
	'Get Log In text from browser'
	String loginText = WebUI.getText(findTestObject('Object Repository/Toastmasters-Login/h1_Log In'))
	Thread.sleep(1000)
	
	'Compare Lon In'
	if (loginText == "Log In")
	{
		GlobalVariable.vartestresult="PASS"
		GlobalVariable.varpassremarks="Login page load successfully & Page name is: "+loginText
		} else {
		GlobalVariable.vartestresult="FAIL"
		GlobalVariable.varfailremarks="Login page not loaded successfully."
	}
	
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)
    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
    
}
catch (Exception e) {
    println(e)
    GlobalVariable.vartestresult = 'FAIL'
    GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)
    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
}