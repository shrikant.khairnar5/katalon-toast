import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.By as By
import org.openqa.selenium.WebElement as WebElement
import java.net.URL as URL
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap.MapEntry

import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.WebElement
import com.kms.katalon.core.webui.common.WebUiCommonHelper
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import org.openqa.selenium.By;


try {
    CustomKeywords.'com.application.DateTime.startTime'()

	WebUI.openBrowser("https://tmcmstest2.toastmasters.org/footer/faq")
	//WebUI.openBrowser("https://www.toastmasters.org/footer/faq")
	
	Thread.sleep(2000)
		
	String url = "";
	int respCode = 200;
	HttpURLConnection huc = null;
	
	WebDriver driver = DriverFactory.getWebDriver()
	
	List<WebElement> links = driver.findElements(By.xpath("//a[starts-with(@href, '/')]"));
	Iterator<WebElement> it = links.iterator();
	
	
	//Map<String,String> urlPair  = new HashMap();
	ArrayList<String> al=new ArrayList<String>();
	ArrayList<String> bl=new ArrayList<String>();
	
	while(it.hasNext()){
		
		url = it.next().getAttribute("href");
		System.out.println(url);
		if(url == null || url.isEmpty()){
			System.out.println("URL is either not configured for anchor tag or it is empty");
							continue;
						}
		
		try {
			huc = (HttpURLConnection)(new URL(url).openConnection());
			huc.setRequestMethod("HEAD");
			huc.connect();
			respCode = huc.getResponseCode();
			println "Resp code is: "+respCode
			
			if(respCode >= 400){			
				GlobalVariable.vartestresult = 'FAIL'
				al.add(url);
				bl.add(respCode);
				GlobalVariable.varfailremarks = 'Broken URL is: '+url
				
				
				//urlPair.put(url, 'FAIL');
			}
			else{
				System.out.println(url+" is a valid link");
				//urlPair.put(url, 'PASS');
			}
				
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	System.out.println("New Code Data: "+al);
	System.out.println("Response Code Data: "+bl);
	//for (Map.Entry<String,String> entry : urlPair.entrySet())
	//System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
	    
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)
    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
}
catch (Exception e) {
    println(e)
    GlobalVariable.vartestresult = 'FAIL'
    GlobalVariable.varfailremarks = ('Exception: ' + e.getStackTrace().toString())
    CustomKeywords.'com.application.DPOSCustomKeyword.settestresult'(GlobalVariable.vartestresult, GlobalVariable.varpassremarks, 
    GlobalVariable.varfailremarks)

    CustomKeywords.'com.application.DateTime.endTime'()
    CustomKeywords.'com.application.DateTime.totalTime'()
}