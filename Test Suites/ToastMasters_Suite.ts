<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ToastMasters_Suite</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>99b8c1f5-da20-47db-bc63-d52f5231903e</testSuiteGuid>
   <testCaseLink>
      <guid>0248a90b-eb01-44c8-992b-2550758d9761</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC001_OpenApplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a2a528f1-0f53-4dbc-ae84-4164e6e85b1d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC002_LoginPage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>45409ec2-3ac1-41ec-8126-558d53917d5f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC003_BlankCredential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc25fce8-fd58-4922-8fe9-0566578e37d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC004_InvalidCredential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ded1baf4-1d4f-4605-9c1e-1bfb2349062e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC005_ValidCredential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a878bc86-1215-4dbf-8c75-c2dd5db74ae1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC001_ClickProfile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab5b2bf9-fa67-440e-be8c-155f84478cb0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC002_ClickClubSupport</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>88469ddc-803d-4981-8c0e-fb5971acd718</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC033_SearchValidClubHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9f81d0ae-1cd4-4217-9b8c-91e93d08b861</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC034_SearchInvalidClubHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4bea93b0-e9d7-4b4f-b0fd-69ebe40311d7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC003_ClickMembership</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3ed299a4-359c-4e07-ab3d-74e8582219b0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC027_SearchValidMembershipHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>90092c43-6477-4ac6-858e-2259f3dea490</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC028_SearchInvalidMembershipHistory</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>911ef2df-e8f7-4a80-8e01-bdbc421e03c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC004_ClickMagazine</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3de9d47f-de36-4e6c-9b94-e15fc50c509c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC007_OfficeHeld</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fe8fa552-6780-4582-981f-90d56cbd7d3e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC029_SearchNumberInOfficeHeld</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7cdba3e3-57c9-4111-9125-25d5196abc58</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC030_SearchInvalidNumberOfficeHeld</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7d5a984c-93de-4b91-af34-3a93e2bff3c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC006_ClickApplicationOrganize</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>77afefa7-99b5-4d0e-8519-260c31e4ac01</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC005_ClickAdditionRole</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9ee4b380-766a-4d92-bdbd-0cdd346c5d39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC035_SearchInvalidGeneralAchievements</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>12f5812e-05cf-4ebf-a959-115a26877fa3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC036_SearchInvalidServiceAchievements</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>69d3b881-f86b-4ab9-b282-85d4484a8e41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC037_SearchInvalidSpeechContestAwards</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5235878e-ea98-4f3b-99d4-8fc12cbfbc2e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC008_ClickMyDownload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6cdf9dcf-eb55-4734-af31-ba2892daa56f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC031_SearchProductInMyDownload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>563e9ba0-e3a1-4509-9b22-987e2536fd29</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC032_SearchInvalidProductInMyDownload</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e6ad1dd-0104-47e3-b304-e51c8d7afa20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC009_ClickSponsorship</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>836682d7-e319-42b2-8f01-349350a498ef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC023_SearchSponsorshipList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8fa057eb-fdad-449d-ac64-42c575fa4304</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC024_SearchInvalidMemberId</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ef68ccfb-edeb-41d3-b6a1-63b5e0237e79</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC025_SearchAwardsList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>352f1874-8256-4113-a8db-a42ac985025b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC026_SearchInvalidAwardName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9a1bcdaa-be2d-4b16-9bb3-ad4ece434362</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC010_ClickPrivacy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>be68264b-c4f0-482f-9b73-7bf2b36db30e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC011_ClickProxyInfo</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5985af6c-b83d-4b1c-b2e4-2b1f38cf5f05</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC012_ClickEducationAward</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f2053fd-81a0-44e2-9e88-fcd2a1f1aa5e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC019_FilterUsingSelectProgram</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7c4b960a-d9bf-4e3c-bbd0-394df465205e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC020_FilterUsingSelectClub</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b0f8af78-4392-46ef-9dcf-a9b56e0a67a6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC021_SearchUsingKeyword</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>570a796c-e9b1-405f-8fc5-adc6b0734b39</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Profile/TC022_ClearFilterFunctionality</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e114ec5b-98c3-4781-9bdd-6fea090a2834</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC013_EditContactInvalidNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e93e9eda-c7ca-4a5e-af70-f19b41445f48</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC014_EditContactValidNumber</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>de7b5814-4c38-4fb6-87ee-bbced50cc2b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login/TC006_FindLinks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cc20c8f6-a73c-4968-9dbe-6cd6c0f02369</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC015_ClickOnSetProfilePhoto</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c9f04098-6a25-4044-96bc-5de3248d8bda</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC016_UploadInvalidFile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c75c0826-ece3-47ab-a80a-28b84be605ad</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC017_UploadValidFile</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
