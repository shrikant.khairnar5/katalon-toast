<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ToastMasters</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>afdc3c8a-4e2a-433b-9a7b-cceb23788735</testSuiteGuid>
   <testCaseLink>
      <guid>0248a90b-eb01-44c8-992b-2550758d9761</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC001_OpenApplication</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85fada6d-ccd9-4346-800a-e6964e88e085</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC002_LoginPage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dc9f6c59-482a-43ef-b04a-7f900e65e8c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Login/TC005_ValidCredential</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>75fcb5c1-27f0-47c5-87f4-ec90b1b72921</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC001_ClickonAllAboutToastMasterLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d3970a7d-f6cd-4a83-806f-8b67839b75c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC002_ClickOnSmedleyFund</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>27527e17-5876-41dc-a0ab-be1040666e8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC003_ClickOnOurMission</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fd555d82-ceec-4500-9a55-231ac2852648</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC004_ClickOnHistoryLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2a040db0-716d-4664-a835-acb9d13e9c60</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC005_ClickOnWorldHeadquarters</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1eff6ba7-903b-4173-9015-58533aa8b970</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC006_ClickOnInternationalPresidentsLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eec44d9f-03ea-4ad9-b031-44fe9eb5adde</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC007_ClickOnBoardDirectors</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8814c36d-4776-45a6-a747-75bff32f982e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC008_ClickOnBoardOfDirectors</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a42684c5-4408-4414-9ef6-2f623da70eb6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC009_ClickOnMeetingMinutes</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e3856d1a-625f-4084-9c49-896fc4349f92</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC010_ClickOnOfficerCandidates</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0a0eee22-0d77-4309-9352-023609162844</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC011_ClickOnDirectorCandidates</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c475973c-1dcd-4470-aae9-e0b26868d086</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC012_ClickOnAdvisoryCommittee</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ca9ed45-bb60-4d2a-b81a-42c104e33960</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC013_ClickOnCandidateQualifications</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>226e3177-2daf-4359-b863-360d49a1c265</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC014_ClickOnLeadershipCommittee</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9b4908bf-0443-41dd-b764-3a4e38458568</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC015_ClickOnBoardBriefing</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13b7d93c-289a-4208-90a1-fdc7ad5e7c64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC016_ClickOnLeadershipRoles</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6372bb31-59b8-45a3-8608-3d1ef1e65bb3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC017_ClickOnCandidacyInformation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f4843357-90a5-4c8d-b93d-45f647adfb47</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC018_ClickOnPartner</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1812f748-c9c7-446b-b766-9e8deac0b64d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC019_ClickOnMediaCenter</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4a3f0dbb-a161-423c-84eb-9c7e338a79ce</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC020_ClickOnNewsReleases</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d045980f-500a-4674-b5e1-dfb530b74c11</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC021_ClickOnInTheNewsLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>abc4f0f7-4bfe-48b1-ab39-b6b93fa3ebe5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC022_ClickOnMediaKitLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acb9545b-9f4a-4eaa-a8ad-ba51e5b54bee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/About/TC023_ClickOnVideoLink</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>39fcd2ea-fd2d-4660-815f-3fc62be3c23f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC001_ClickOnPrivacyPolicy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c0e56041-db87-45fa-acdd-a33f44b92219</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC002_ClickOnConditionsOfUse</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b3060d5f-6cd6-4c84-a939-e812394c1be7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC003_ClickOnBrowserCompatibility</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4ba7d602-81c3-4c5e-a25c-c019da19ccc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC004_ClickOnCopyright</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9c20d718-c5d2-42e2-8753-966c5408c84f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC005_ClickOnSiteMap</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1e6d2527-f452-45e1-ade8-9d8c123c5fee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC006_ClickOnAboutInSiteMap</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ca4266b-8acf-40cf-b896-d73e2ce45ae4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC007_ClickOnLeadershipCentralSiteMap</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>871aefdc-9cb4-4b17-a350-735009b6cc73</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC008_ClickOnEducationInSiteMap</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2d56f59a-10c3-4c36-b486-ff0c00f4fc3a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC009_ClickOnStartClub</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ce910273-0003-4acf-b874-b8daf2eefec3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC010_ClickOnMagazine</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>68aee97c-7c97-4c2e-8de1-b3099979345e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC011_ClickOnMembership</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b64a67a-9db8-4e88-b98e-592f4264bc7f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC012_ClickOnResources</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>593a992f-d94f-4c9b-8e54-424b159d83f6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC014_ClickOnShop</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>53555ced-5bab-4b98-81f9-510f07ccdf9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC015_ClickOnFAQ</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b76da365-ffc6-4dd0-91ab-e0a340793600</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC016_ClickOnALSDTMExtension</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d756d0a6-685b-49d3-b75a-222cee2b9683</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC017_ClickOnGDPR</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4f93f369-902f-433f-8f1c-dcedd20a335a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC018_ClickOnSalesTax</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b8e945ce-027c-484f-acc8-dc05ee306d03</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC019_ClickOnWowFactor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05f12e39-4462-4e84-8db1-75ecb9074d07</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC020_ClickOnClubBusiness</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>aa344677-ab38-44a4-a1aa-872effe0629b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC021_ClickOnDistinguishedClubProgram</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>3b34f0b3-a15e-4316-b046-79bb417967d2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC022_ClickOnDistrictEvents</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e743df95-72e8-4ff0-ad24-015e8166c200</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC023_ClickOnDuesIncrease</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>92a6bb3f-489e-4c33-bce1-3a145fbe5873</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC013_ClickOnFindAClub</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1212a598-7a0b-4025-ad39-34ceec02f6de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC025_ClickOnMembership</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1205f811-a389-4406-b3e3-2848a35c7d98</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC026_ClickOnNewClubs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e2786b60-fc52-4d4f-abe6-af6b688caaa4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC027_ClickOnOnlineClubs</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2b906559-7c9a-4684-bbc9-0099675a7208</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC028_ClickOnPathwaysAccessibleMaterials</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>37dff4e2-79b7-4fd5-a781-db6bd2ed80c5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC029_ClickOnProxy</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>06bc6fa6-8bd4-46eb-b796-507c283071eb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC030_ClickOnRegionAdvisor</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c1daffd5-9a98-4bf2-97ca-5b512129e6b6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Login/TC006_FindLinks</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>333bfaf3-2af4-47ab-b1ad-52b6b9a21537</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC017_UploadValidFile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6532ffda-f435-4fe3-9967-41a4fc685f9b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC046_DeleteProfilePhoto</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6a409ecc-8304-4246-b575-6308f0d8ae9c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Footer/TC024_ClickOnGlobalCompliance</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>48826947-a2fd-4369-b789-5d235efad023</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC016_UploadInvalidFile</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>33be0407-d265-4286-a50f-8186751c2c46</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC047_ClickOnChooseAPath</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>068d17df-8ecc-4fdd-8206-57c4c2af15af</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC048_SelectMaterial</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>85b1139d-6bcc-4c67-bc83-79cc08dd99f4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC049_SelectLearningPath</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40db8580-4bdc-45de-b8ae-9803ccea9d0e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC050_SelectPath</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5dbbecd8-d084-453d-8547-eda439e1f7c9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC051_CheckoutPageAddressWithBlankValue</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08c5f8d5-8fba-4aba-9564-b5d40ca05192</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC052_CheckoutPage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65ed0486-d3ec-4953-918f-f5f979de6906</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Profile/TC053_PaymentMethod</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a9a6d015-0508-4a3c-9c78-97d18f4b9b0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add Membership/TC001_OpenAddMembershipModule</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e6fd563-fd8b-4097-82e2-f43b6beac531</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Add Membership/TC002_MembershipPage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2c890ff4-4c73-45d4-9252-e9294f83e564</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC003_SelectYesNewMember</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d9ff930b-eca7-4941-9dbd-bc3f986cb98c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC004_VerifyFieldValidation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>374f11b0-abde-4a79-87fa-3db23c91ab41</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC005_FillupFormWithValidInput</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbb1faae-1588-46d4-b7dd-d35eafcf0336</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC007_SearchReinstatedMemberWithValidData</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ab332e06-4c73-4a56-af2c-b7f0ec3e5288</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC006_SearchReinstatedMemberWithInValidData</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>4cea48e4-244a-4fb6-94bb-0aaf8e4f5fda</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>59e8d556-1bcc-4106-87e1-d17fa92c5208</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6a479614-b3ff-45f5-a3a2-1d683c9d82cd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC009_Submit Payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>946a1e1c-c523-4ca6-be73-b091d29aecb2</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC010_Select Member for payment</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>31ffd327-1372-4137-a128-dda88fa9800c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC008_VerifyPaymentValidation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e96181c4-bf15-49b8-bbdd-61bc339c1f82</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/Add Membership/TC011_AddPayment Details</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>df06941d-f224-454a-91de-62c0f0f0cf5c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC001_OpenSubmitEductionAwards</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0ffe6ed4-8c28-46cf-bcde-cf1cb5d3861e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC002_SelectMember</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71322cd5-0d4c-460f-9f88-f1c3c447119b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC003_SelectEductionProgram</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4795bbc5-0f59-4fea-8923-09205e1e70a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC004_ReviewAwardSubmission</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7266673a-87e6-4b62-bf58-d816d2f1eb89</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC005_Edit Award</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ac931146-2384-4bbf-a0cc-5d877c8c5ba4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Submit Eduction Awards/TC006_SubmitAward</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
