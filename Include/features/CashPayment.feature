@tag
Feature: The test case verifies that a user can payment with Cash

  Scenario Outline: Cash Payment successfully
    Given The Login page is loaded successfully
    When I login the system with valid  "<articleNo>" username and "<password>" password
    Then The Cash Payment done successfully

    Examples: 
      | articleNo         | password         |
      | 8907249799288 | sPiHQ&YEa6ST`de+ |