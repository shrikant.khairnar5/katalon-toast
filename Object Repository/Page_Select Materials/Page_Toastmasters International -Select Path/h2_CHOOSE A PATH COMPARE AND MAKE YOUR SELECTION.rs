<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h2_CHOOSE A PATH COMPARE AND MAKE YOUR SELECTION</name>
   <tag></tag>
   <elementGuidId>1cc833eb-9a37-4b3d-82b8-933a1b3b27f0</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//main[@id='mainContent']/div/div/div/div/h2</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;mainContent&quot;)/div[@class=&quot;container select-path-region en-US&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;pageHead&quot;]/h2[@class=&quot;title&quot;][count(. | //*[(text() = 'CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION' or . = 'CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION')]) = count(//*[(text() = 'CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION' or . = 'CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>h2.title</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h2</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>title</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;mainContent&quot;)/div[@class=&quot;container select-path-region en-US&quot;]/div[@class=&quot;row&quot;]/div[@class=&quot;col-md-12&quot;]/div[@class=&quot;pageHead&quot;]/h2[@class=&quot;title&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//main[@id='mainContent']/div/div/div/div/h2</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Select Path'])[1]/following::h2[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Welcome to Pathways'])[1]/following::h2[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Recommended'])[1]/preceding::h2[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Presentation Mastery'])[1]/preceding::h2[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='CHOOSE A PATH: COMPARE AND MAKE YOUR SELECTION']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//h2</value>
   </webElementXpaths>
</WebElementEntity>
