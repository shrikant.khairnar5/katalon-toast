<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>Validation1</name>
   <tag></tag>
   <elementGuidId>a83068fc-402e-4f7d-a249-b870d09d3063</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='memberInfoForm']/div/div/div/ul/li</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.col-xs-12.col-md-6 > div.form-group.has-error.has-danger > div.help-block.with-errors > ul.list-unstyled > li</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;memberInfoForm&quot;)/div[@class=&quot;col-xs-12 col-md-6&quot;]/div[@class=&quot;form-group has-error has-danger&quot;]/div[@class=&quot;help-block with-errors&quot;]/ul[@class=&quot;list-unstyled&quot;]/li[1][count(. | //*[(text() = 'Please fill out this field.' or . = 'Please fill out this field.')]) = count(//*[(text() = 'Please fill out this field.' or . = 'Please fill out this field.')])]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>li</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Please fill out this field.</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;memberInfoForm&quot;)/div[@class=&quot;col-xs-12 col-md-6&quot;]/div[@class=&quot;form-group has-error has-danger&quot;]/div[@class=&quot;help-block with-errors&quot;]/ul[@class=&quot;list-unstyled&quot;]/li[1]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='memberInfoForm']/div/div/div/ul/li</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[3]/following::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Last Name (Surname)'])[2]/following::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='First Name'])[1]/preceding::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='*'])[4]/preceding::li[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div/div/div/div/ul/li</value>
   </webElementXpaths>
</WebElementEntity>
