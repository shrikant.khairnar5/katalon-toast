<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>a_Use This Address</name>
   <tag></tag>
   <elementGuidId>21b381fd-b957-48d3-9598-f4da1aa96998</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.ng-scope > a.btn.btn-primary</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='additionalAddressesContainer']/div/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>btn btn-primary</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>ng-click</name>
      <type>Main</type>
      <value>selectAdditionalAddress({&quot;Company&quot;:null,&quot;AdditionalLines&quot;:[&quot;Wally Kaczor III, CL&quot;,null,null],&quot;AttentionLine&quot;:null,&quot;LocationID&quot;:{&quot;Value&quot;:3023147,&quot;Valid&quot;:true},&quot;Phone&quot;:null,&quot;Id&quot;:0,&quot;CustomerId&quot;:null,&quot;IsShippingDefault&quot;:false,&quot;IsBillingDefault&quot;:false,&quot;Name&quot;:&quot;Wally Kaczor III, CL&quot;,&quot;Name2&quot;:null,&quot;Address&quot;:&quot;28842 Curlew Ln&quot;,&quot;Address2&quot;:&quot;&quot;,&quot;Zip&quot;:&quot;92677-1339&quot;,&quot;City&quot;:&quot;Laguna Niguel&quot;,&quot;State&quot;:&quot;CA&quot;,&quot;Country&quot;:{&quot;VatRegion&quot;:null,&quot;Code&quot;:&quot;US&quot;,&quot;Name&quot;:&quot;United States&quot;,&quot;Title&quot;:&quot;United States&quot;,&quot;Alias&quot;:null},&quot;Alias&quot;:null})</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Use This Address</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;additionalAddressesContainer&quot;)/div[@class=&quot;ng-scope&quot;]/a[@class=&quot;btn btn-primary&quot;]</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='additionalAddressesContainer']/div/a</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Use This Address')]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Create New Address'])[1]/following::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Addresses'])[1]/following::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Use This Address'])[2]/preceding::a[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='close'])[2]/preceding::a[2]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Use This Address']/parent::*</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[5]/div/div[2]/div/a</value>
   </webElementXpaths>
</WebElementEntity>
