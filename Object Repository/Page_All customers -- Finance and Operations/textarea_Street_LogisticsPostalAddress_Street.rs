<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>textarea_Street_LogisticsPostalAddress_Street</name>
   <tag></tag>
   <elementGuidId>cbd8c98d-849a-40ae-a6b5-4e199632da03</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//textarea[@id='LogisticsPostalAddress_6_LogisticsPostalAddress_Street_textArea']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>textarea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>multilineInput-textArea field displayoption viewMarker alignmentAuto</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>autocomplete</name>
      <type>Main</type>
      <value>off</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>role</name>
      <type>Main</type>
      <value>textbox</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-multiline</name>
      <type>Main</type>
      <value>true</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dyn-bind</name>
      <type>Main</type>
      <value>
        attr: { 
            name: $data.Name,
            rows: $data.DisplayHeight,
            placeholder: $data.PlaceHolder,
            'aria-describedby': $data.Id + '_helptext',
            'aria-invalid': !$dyn.value($data.IsValid),
        },
        ariaLabelledBy: { byElementId: $data.Id + '_label', showLabel: $data.ShowLabel, isInGrid: $data._isInGrid },
        boundValue: { binder: $data.valueBinder, changeMode: $data.ChangeMode, allowEnter: true },
        id: $data.Id + '_textArea',
        link: $dyn.ui.link($data),
        secondaryNavigation: {keyboard: $data.OnKeyboardSecondaryNavigation, mouse: $data.OnMouseSecondaryNavigation},
        title: $data.toolTip,
        sizing: {height: ($dyn.value($data.Height) == $dyn.layout.Size.available ? $dyn.layout.Size.available : $dyn.layout.Size.content),
                 width: ($dyn.value($data.Width) == $dyn.layout.Size.available ? $dyn.layout.Size.available : $dyn.layout.Size.content) },
        skip: $dyn.ui.skip($data),
        required: $data.Required,
        allowEdit: $data.AllowEdit,
        enabled: $data.Enabled,
        maxlength: $data.LimitText,
        textAlign: $data.Alignment,
        keyDown: $data.keyDown,
        input: $data.onInput,
        focusIn: $data.focusInCallBack,
        blur: $data.focusOutCallBack,
        foregroundColor: $data.ForegroundColor,
        backgroundColor: $data.BackgroundColor,
        css: { 'validationFailed': $data.IsShowingWarningIndicator }</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>LogisticsPostalAddress_Street</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>rows</name>
      <type>Main</type>
      <value>5</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-describedby</name>
      <type>Main</type>
      <value>LogisticsPostalAddress_6_LogisticsPostalAddress_Street_helptext</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-invalid</name>
      <type>Main</type>
      <value>false</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>aria-labelledby</name>
      <type>Main</type>
      <value>LogisticsPostalAddress_6_LogisticsPostalAddress_Street_label</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>LogisticsPostalAddress_6_LogisticsPostalAddress_Street_textArea</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tabindex</name>
      <type>Main</type>
      <value>0</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>maxlength</name>
      <type>Main</type>
      <value>250</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data-dynamics-input-alignment</name>
      <type>Main</type>
      <value>Auto</value>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;LogisticsPostalAddress_6_LogisticsPostalAddress_Street_textArea&quot;)</value>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//textarea[@id='LogisticsPostalAddress_6_LogisticsPostalAddress_Street_textArea']</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='LogisticsPostalAddress_6_LogisticsPostalAddress_Street']/textarea</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Street'])[1]/following::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='ZIP/postal code'])[1]/following::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Building complement'])[1]/preceding::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='City'])[1]/preceding::textarea[1]</value>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[3]/textarea</value>
   </webElementXpaths>
</WebElementEntity>
