package com.application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.annotation.internal.Action
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import internal.GlobalVariable

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement as WebElement
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.winium.DesktopOptions as DesktopOptions
import org.openqa.selenium.winium.WiniumDriver as WiniumDriver
import java.net.MalformedURLException as MalformedURLException
import java.net.URL as URL

public class randomArticle {

	@Keyword
	def ranArticle(){
		def z = ["8907254457241", "8907253507183", "8907254472565"]
		Random rnd = new Random()
		GlobalVariable.randArt = z.get(rnd.nextInt(z.size()))
		println "Select Value is: "+GlobalVariable.randArt
	}


	@Keyword
	def ranData(){
		Random rand = new Random()
		int n=999999;
		int randomNumber = rand.nextInt(n+1)
		GlobalVariable.randData = Integer.toString(randomNumber)
		println "Random number is: "+GlobalVariable.randData
	}


	@Keyword
	def EnterMobile(){

		WiniumDriver driver = new WiniumDriver()

		//GlobalVariable.WinDriver = driver
		driver.findElement(By.name("Continue")).click()
		Thread.sleep(1000)
		String autiID1 = GlobalVariable.WinDriver.findElement(By.name("First To Know")).getAttribute("AutomationId")
		println "Automation ID is: "+autiID1
		String finalStr1 = autiID1.substring(0,6)
		String newStr1 = autiID1.substring(6)
		newIntID1 = Integer.parseInt(newStr1)
		int paymentID1 = newIntID1 + 3
		String srt11 = Integer.toString(paymentID1)
		String str21 = finalStr1.concat(srt11)
		driver.findElement(By.id(str21)).click()
		driver.findElement(By.id(str21)).sendKeys("9420091255")
		Thread.sleep(1000)
		driver.findElement(By.name("Continue")).click()
		Thread.sleep(1000)
	}


	@Keyword
	def CallDriver() {

		WebDriver driver = DriverFactory.getWebDriver()
		Actions myAction = new Actions(driver)

		//WiniumDriver driver = null
		String appPath = 'C:/Windows/System32/cmd.exe'
		DesktopOptions option = new DesktopOptions()
		option.setApplicationPath(appPath)
		option.setDebugConnectToRunningApp(false)
		option.setLaunchDelay(2)
		driver = new WiniumDriver(new URL('http://localhost:9999'),  option)
		Thread.sleep(2000)
		driver.close()
	}
}
