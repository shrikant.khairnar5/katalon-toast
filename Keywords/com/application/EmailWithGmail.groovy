package com.application

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.google.api.client.util.IOUtils
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable
import java.util.Properties
import javax.activation.DataSource
import javax.mail.Message
import javax.mail.Multipart
import javax.mail.Session
import javax.mail.internet.MimeBodyPart
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;


import java.io.FileInputStream
import java.io.FileOutputStream
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import java.lang.String;

public class EmailWithGmail {
	//public static void main(String [] args) {

	@Keyword
	def testemail (String [] args) {
		//String to = "shrikant.khairnar@futuregroup.in";
		String msg = '';
		msg += 'Dear Team, \n\n\n';
		//msg += 'Please find the attached Test Result. \n\n\n';
		msg += 'Regards, \n';
		msg += 'DnT Team\n';
		String subject = "[All_Master] [API Results]"+ GlobalVariable.ExecDateTime
		//final String from ="shrikant.iqr@gmail.com"
		//final  String password ="Test!@12"


		Properties props = new Properties();

		props.setProperty("mail.smtp.host", "smtp.gmail.com");
		props.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.setProperty("mail.smtp.socketFactory.fallback", "false");
		props.setProperty("mail.smtp.port", "465");
		props.setProperty("mail.smtp.socketFactory.port", "465");
		props.put("mail.smtp.auth", "true");
		props.put("mail.debug", "true");
		props.setProperty("mail.transport.protocol", "smtp");
		Session session = Session.getDefaultInstance(props,
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from,password);
					}
				});
		try {
			// Create a default MimeMessage object.
			//session.setDebug(true);
			Transport transport = session.getTransport();
			InternetAddress addressFrom = new InternetAddress(from);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(addressFrom);
			message.setSubject(subject);

			// Create the message part
			BodyPart messageBodyPart = new MimeBodyPart();

			// Now set the actual message
			//messageBodyPart.setText(msg);

			// Create a multipar message
			Multipart multipart = new MimeMultipart();

			// Set text message part
			//multipart.addBodyPart(messageBodyPart);

			BodyPart htmlPart = new MimeBodyPart();
			htmlPart.setContent(body, "text/html;charset=UTF-8");

			// Part two is attachment
			messageBodyPart = new MimeBodyPart();
			String filepath = GlobalVariable.TestResultFile;
			//String fileAttachment = GlobalVariable.G_AppPath+"/Data Files/BluPayClarks_TestResult_Phase1.xlsx";

			println (filepath)
			DataSource source = new FileDataSource(filepath);
			String filename = new File(filepath).getName();
			println filename
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(filename);
			multipart.addBodyPart(htmlPart);
			multipart.addBodyPart(messageBodyPart);


			message.setContent(multipart);

			InternetAddress[] iAdressArray = InternetAddress.parse(to);
			message.setRecipients(Message.RecipientType.TO, iAdressArray);
			transport.connect();
			Transport.send(message);
			transport.close();
			System.out.println("Sent message successfully....");
		} catch (MessagingException mex) {
			mex.printStackTrace();
		}
	}


	/*
	 public static FileOutputStream outputFile;
	 static FileInputStream file;
	 //	public static final String TEST_CASES = "E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx";
	 public static final String TEST_CASES = GlobalVariable.TestResultFile;
	 int rowCount;
	 String xl = TEST_CASES;
	 String sheet = "Summary";
	 @Keyword
	 def testingHTML() {
	 String th="border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #5b9bd5;color: white;";
	 StringBuffer pw = new StringBuffer();
	 //PrintWriter pw = new PrintWriter(new FileWriter("E:\\Katalon_project\\Katalon_mobile\\Demo\\Data Files\\TestReport.html"));
	 pw.append("<html><body>Dear Team,<BR/><BR/>Please find the attached Test Result for the last "+GlobalVariable.Phase+" execution on below environment.<BR/><BR/>")
	 pw.append(
	 "<h3>"+GlobalVariable.ProjectName+""+GlobalVariable.Phase+" Automation Test Execution Report Summary</h3>");
	 pw.append("<head><TABLE style='font-family:Georgia, serif;border-collapse: collapse;width:80%;border:1px solid #ddd;padding:8px;'><TR style='background-color: #ddd';><TH style='"+th+"'>ModuleName<TH style='"+th+"'>NoOfTestCases<TH style='"+th+"'>PASS<TH style='"+th+"'>FAIL</TR>");
	 rowCount = getRowCount(xl, sheet)
	 System.out.println(rowCount);
	 String css = "background-color: #f2f2f2;";
	 String td="border: 1px solid #ddd;padding: 8px;";
	 for (int i = 1; i <= rowCount; i++) {
	 String moduleName = getCellValue(xl, sheet, i, 0);
	 System.out.println(moduleName);
	 String noOfTestCases = getCellValue(xl, sheet, i, 1);
	 String passCount = getCellValue(xl, sheet, i, 2);
	 String failCount = getCellValue(xl, sheet, i, 3);
	 pw.append("<TR style='"+css+"'><TD style='"+td+"'>" + moduleName + "<TD style='"+td+"'>" + noOfTestCases + "<TD style='"+td+"'>" + passCount + "<TD style='"+td+"'>" + failCount);
	 }
	 pw.append("</TABLE></head><BR/>Regards,<BR/> DnT </body></html>");
	 //pw.close();
	 return pw;
	 }
	 */

	/*
	 @Keyword
	 def getCellValue(String xl, String sheet, int row, int cell) {
	 def val = "";
	 try {
	 FileInputStream file = new FileInputStream (xl);
	 XSSFWorkbook workbook = new XSSFWorkbook(file);
	 XSSFCell cell1 = workbook.getSheet(sheet).getRow(row).getCell(cell);
	 switch(cell1.getCellType()){
	 case XSSFCell.CELL_TYPE_NUMERIC:
	 val = cell1.getNumericCellValue() + "";
	 case XSSFCell.CELL_TYPE_STRING:
	 val = cell1.getStringCellValue() + "";
	 case XSSFCell.CELL_TYPE_FORMULA:
	 XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
	 val = cell1.getNumericCellValue() + "";
	 return val.substring(0, val.indexOf('.'));
	 file.close();
	 }
	 }catch(Exception e){
	 println(e.getStackTrace().toString())
	 }
	 return val;
	 }
	 @Keyword
	 def getRowCount(String xl, String sheet) {
	 try {
	 FileInputStream files = new FileInputStream(xl);
	 XSSFWorkbook workbook = new XSSFWorkbook(files);
	 return workbook.getSheet(sheet).getLastRowNum();
	 } catch (Exception e) {
	 return 0;
	 }
	 }
	 @Keyword
	 def getRowCount(String sheet, int row, int cell, String result) {
	 //	FileInputStream file = new FileInputStream("E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx");
	 FileInputStream file = new FileInputStream(GlobalVariable.TestResultFile);
	 XSSFWorkbook workbook = new XSSFWorkbook(file);
	 XSSFSheet sheets = workbook.getSheet(sheet);
	 FileOutputStream output = new FileOutputStream(GlobalVariable.TestResultFile);
	 //	FileOutputStream output = new FileOutputStream("E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx");
	 sheets.getRow(row).createCell(cell).setCellValue(result);
	 workbook.setForceFormulaRecalculation(true);
	 workbook.write(output);
	 output.close();
	 }
	 }
	 */


	/*
	 import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
	 import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
	 import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
	 import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
	 import com.google.api.client.util.IOUtils
	 import com.kms.katalon.core.annotation.Keyword
	 import com.kms.katalon.core.checkpoint.Checkpoint
	 import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
	 import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
	 import com.kms.katalon.core.model.FailureHandling
	 import com.kms.katalon.core.testcase.TestCase
	 import com.kms.katalon.core.testdata.TestData
	 import com.kms.katalon.core.testobject.TestObject
	 import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
	 import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
	 import internal.GlobalVariable
	 import java.util.Properties
	 import javax.activation.DataSource
	 import javax.mail.Message
	 import javax.mail.Multipart
	 import javax.mail.Session
	 import javax.mail.internet.MimeBodyPart
	 import java.io.FileInputStream
	 import java.io.FileOutputStream
	 import java.util.*;
	 import javax.mail.*;
	 import javax.mail.internet.*;
	 import javax.activation.*;
	 import org.apache.poi.xssf.usermodel.XSSFCell;
	 import org.apache.poi.xssf.usermodel.XSSFFormulaEvaluator
	 import org.apache.poi.xssf.usermodel.XSSFRow;
	 import org.apache.poi.xssf.usermodel.XSSFSheet;
	 import org.apache.poi.xssf.usermodel.XSSFWorkbook;
	 import java.lang.String;
	 public class EmailwithGmail {
	 @Keyword
	 def testemail (String [] args) {
	 //	public static void main(String [] args) {
	 final String username = "shrikant.iqr@gmail.com";
	 final  String password ="Test!@12"
	 final String to = "shrikant.khairnar@futuregroup.in";
	 final String subject = "["+GlobalVariable.ProjectName+"] ["+GlobalVariable.Phase+" Execution Result]"+ GlobalVariable.ExecDateTime
	 String body = testingHTML();
	 Properties props = new Properties();
	 props.setProperty("mail.smtp.host", "smtp.gmail.com");
	 props.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
	 props.setProperty("mail.smtp.socketFactory.fallback", "false");
	 props.setProperty("mail.smtp.port", "465");
	 props.setProperty("mail.smtp.socketFactory.port", "465");
	 props.put("mail.smtp.auth", "true");
	 props.put("mail.debug", "true");
	 props.setProperty("mail.transport.protocol", "smtp");
	 Session session = Session.getInstance(props,
	 new javax.mail.Authenticator() {
	 protected PasswordAuthentication getPasswordAuthentication() {
	 return new PasswordAuthentication(username, password);
	 }
	 });
	 try {
	 Message message = new MimeMessage(session);
	 message.setFrom(new InternetAddress(username));
	 message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to));
	 message.setSubject(subject);
	 BodyPart messageBodyPart = new MimeBodyPart();
	 Multipart multipart = new MimeMultipart();
	 BodyPart htmlPart = new MimeBodyPart();
	 htmlPart.setContent(body, "text/html;charset=UTF-8");
	 String filepath = GlobalVariable.TestResultFile;
	 //	String filepath = "E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx";
	 DataSource source = new FileDataSource(filepath);
	 messageBodyPart.setDataHandler(new DataHandler(source));
	 String sendFileName = new File(filepath).getName();
	 messageBodyPart.setFileName(sendFileName);
	 multipart.addBodyPart(messageBodyPart);
	 multipart.addBodyPart(htmlPart);
	 message.setContent(multipart);
	 Transport.send(message);
	 System.out.println("Sent message successfully....");
	 } catch (MessagingException e) {
	 e.printStackTrace();
	 }
	 }
	 public static FileOutputStream outputFile;
	 static FileInputStream file;
	 //	public static final String TEST_CASES = "E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx";
	 public static final String TEST_CASES = GlobalVariable.TestResultFile;
	 int rowCount;
	 String xl = TEST_CASES;
	 String sheet = "Summary";
	 @Keyword
	 def testingHTML() {
	 String th="border: 1px solid #ddd;padding: 8px;padding-top: 12px;padding-bottom: 12px;text-align: left;background-color: #5b9bd5;color: white;";
	 StringBuffer pw = new StringBuffer();
	 //PrintWriter pw = new PrintWriter(new FileWriter("E:\\Katalon_project\\Katalon_mobile\\Demo\\Data Files\\TestReport.html"));
	 pw.append("<html><body>Dear Team,<BR/><BR/>Please find the attached Test Result for the last "+GlobalVariable.Phase+" execution on below environment.<BR/><BR/>")
	 pw.append("<b>Manufacture: </b>"+GlobalVariable.Manufacturar+"<BR/>")
	 pw.append("<b>Device Name: </b>"+GlobalVariable.Device_Name+"<BR/>")
	 pw.append("<b>Android Version: </b>"+GlobalVariable.Android_version+"<BR/>")
	 pw.append("<b>APK Version: </b>"+GlobalVariable.APK_version+"<BR/>")
	 pw.append(
	 "<h3>"+GlobalVariable.ProjectName+""+GlobalVariable.Phase+" Automation Test Execution Report Summary</h3>");
	 pw.append("<head><TABLE style='font-family:Georgia, serif;border-collapse: collapse;width:80%;border:1px solid #ddd;padding:8px;'><TR style='background-color: #ddd';><TH style='"+th+"'>ModuleName<TH style='"+th+"'>NoOfTestCases<TH style='"+th+"'>PASS<TH style='"+th+"'>FAIL</TR>");
	 rowCount = getRowCount(xl, sheet)
	 System.out.println(rowCount);
	 String css = "background-color: #f2f2f2;";
	 String td="border: 1px solid #ddd;padding: 8px;";
	 for (int i = 1; i <= rowCount; i++) {
	 String moduleName = getCellValue(xl, sheet, i, 0);
	 System.out.println(moduleName);
	 String noOfTestCases = getCellValue(xl, sheet, i, 1);
	 String passCount = getCellValue(xl, sheet, i, 2);
	 String failCount = getCellValue(xl, sheet, i, 3);
	 pw.append("<TR style='"+css+"'><TD style='"+td+"'>" + moduleName + "<TD style='"+td+"'>" + noOfTestCases + "<TD style='"+td+"'>" + passCount + "<TD style='"+td+"'>" + failCount);
	 }
	 pw.append("</TABLE></head><BR/>Regards,<BR/>Test COE</body></html>");
	 //pw.close();
	 return pw;
	 }
	 @Keyword
	 def getCellValue(String xl, String sheet, int row, int cell) {
	 def val = "";
	 try {
	 FileInputStream file = new FileInputStream (xl);
	 XSSFWorkbook workbook = new XSSFWorkbook(file);
	 XSSFCell cell1 = workbook.getSheet(sheet).getRow(row).getCell(cell);
	 switch(cell1.getCellType()){
	 case XSSFCell.CELL_TYPE_NUMERIC:
	 val = cell1.getNumericCellValue() + "";
	 case XSSFCell.CELL_TYPE_STRING:
	 val = cell1.getStringCellValue() + "";
	 case XSSFCell.CELL_TYPE_FORMULA:
	 XSSFFormulaEvaluator.evaluateAllFormulaCells(workbook);
	 val = cell1.getNumericCellValue() + "";
	 return val.substring(0, val.indexOf('.'));
	 file.close();
	 }
	 }catch(Exception e){
	 println(e.getStackTrace().toString())
	 }
	 return val;
	 }
	 @Keyword
	 def getRowCount(String xl, String sheet) {
	 try {
	 FileInputStream files = new FileInputStream(xl);
	 XSSFWorkbook workbook = new XSSFWorkbook(files);
	 return workbook.getSheet(sheet).getLastRowNum();
	 } catch (Exception e) {
	 return 0;
	 }
	 }
	 @Keyword
	 def getRowCount(String sheet, int row, int cell, String result) {
	 //	FileInputStream file = new FileInputStream("E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx");
	 FileInputStream file = new FileInputStream(GlobalVariable.TestResultFile);
	 XSSFWorkbook workbook = new XSSFWorkbook(file);
	 XSSFSheet sheets = workbook.getSheet(sheet);
	 FileOutputStream output = new FileOutputStream(GlobalVariable.TestResultFile);
	 //	FileOutputStream output = new FileOutputStream("E:/Katalon_project/Katalon_mobile/Demo/Data Files/DemoMobile_TestResults.xlsx");
	 sheets.getRow(row).createCell(cell).setCellValue(result);
	 workbook.setForceFormulaRecalculation(true);
	 workbook.write(output);
	 output.close();
	 }
	 }
	 */

}